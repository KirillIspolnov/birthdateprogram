package net.kivitechnologies.BrainWork.BirthDateProgram;

import java.util.Date;

/**
 * �����, ����������� �������� ����� main
 *  
 * @author ��������� ������, 16��18�
 */
public class BirthDateMain {
	private static Date birthDate, todayDate;
	
	/**
	 * �������� �����
	 * 
	 * @param args ������ ��������� ����������, ���������� ����� ��������� ������ ��� ��������
	 */
	public static void main(String[] args) {
		birthDate = IOMethods.getBirthDate();
		todayDate = IOMethods.getTodayDate();
		
		flushYears();
		
		if(birthDate.after(todayDate)) {
			IOMethods.write("� �����������!");
		} else if(birthDate.before(todayDate)) {
			IOMethods.write("� ���������!");
		} else {
			IOMethods.write("� ���� ��������! �����, �������� � �������! ���!");
		}
	}

	/**
	 * ������������� � ���� �������� Date ���������� ����
	 * ���������� ��� ����������� ����������� ���� ��� ��������� ������ ��� ����� ������� ����
	 */
	@SuppressWarnings("deprecation")
	private static void flushYears() {
		birthDate.setYear(todayDate.getYear());
	}
}
