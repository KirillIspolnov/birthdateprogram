package net.kivitechnologies.BrainWork.BirthDateProgram;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * �����, ����������� ������ ��� ������ � ������:
 * <ul>
 * <li>�������� �� ������������</li>
 * <li>�������������� ������ � ����</li>
 * <li>��������� ����, ������������� �� ������������ ����������</li>
 * </ul>
 * 
 * @author ��������� ������, 16��18�
 */
public class DateMethods {
	public static final SimpleDateFormat DATE_FORMAT_POINTS = new SimpleDateFormat("dd.MM.yyyy");
	/**
	 * ���������� true, ���� ���� ������� ������ �� ���� � ������������� ������� **.**.****
	 * 
	 * @param date ���� � ���� ������
	 * @return true, ���� ���������� ������ ������������� ������� **.**.****, ��� * - ����������� �����
	 */
	public static boolean isDateValid(String date) {
		String[] parts = date.split("[.]");
		
		boolean valid = parts[0].matches("\\d\\d");
		valid = valid && parts[1].matches("\\d\\d");
		valid = valid && parts[2].matches("\\d\\d\\d\\d");
		
		int day = Integer.parseInt(parts[0]);
		int month = Integer.parseInt(parts[1]);
		int year = Integer.parseInt(parts[2]);
		int max = 30;
		if(month == 2)
			max = year % 4 == 0 ? 29 : 28;
		else if(parts[1].matches("01|03|05|07|08|10|12"))
			max = 31;

		valid = valid && month < 13;
		valid = valid && day <= max;
		
		return valid;
	}
	
	/**
	 * ���������� true, ���� ���� ����� ������ **.**.****, ����� false
	 * �������� ����� - ���������� ��������, ������� �� ���� ������ �� ����
	 * � �������, �������� aa.bb.cccc ������ true 
	 * 
	 * @param date ���� � ���� ������
	 * @return true, ���� ���������� ������ ������������� ������� **.**.****
	 */
	public static boolean isFormatValid(String date) {
		String parts[] = date.split("[.]");
		
		return parts[0].length() == 2 & parts[1].length() == 2 && parts[2].length() == 4;
	}
	
	/**
	 * ���������� ������ ������ Date ��������� �� ������ ������, ���������� � ����������
	 * 
	 * @param data ���� � ������ � ������� dd.MM.yyyy
	 * @return ����, ���������� �� ������ � �������������� �������� java.util.Date
	 * @throws ParseException ����������� ������, ���� �� �����-�� ������� ������ ������ ����������
	 */
	public static Date parse(String data) throws ParseException {
		return DATE_FORMAT_POINTS.parse(data);
	}
	
	/**
	 * ���������� ������� ����, ������������� �� ����������
	 * 
	 * @return ����, ���������� � ���������� � �������������� �������� java.util.Date
	 */
	public static Date getTodayDate() {
		return new Date(System.currentTimeMillis());
	}
}
