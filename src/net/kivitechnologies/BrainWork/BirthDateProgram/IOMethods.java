package net.kivitechnologies.BrainWork.BirthDateProgram;

import java.text.ParseException;
import java.util.Date;
import java.util.Scanner;

/**
 * �����, ����������� ������������������ ������ �����-������
 * ��������� ��������� � ��������� ������� � ���� ��� �� ������ �����
 * 
 * @author ��������� ������, 16��18�
 *
 */
public class IOMethods {
	public static final String WRITE_YOUR_BIRTH_DATE     = "������� ���� ������ ��� �������� � ������� ��.��.���� (�� ����� ����� ����� ����� ����: / � -): ",
			                   INCORRECT_DATE            = "��, ������, ��� ����� ���� �� ��������� ������. ���������� ��� ���: ",
			                   PARSING_EXCEPTION_THROWED = "��������� ����������� ������! � ���������, ��������� �������� �������� ���� ������!",
			                   IS_TODAY_CORRECT          = "������� %s?\n",
			                   WRITE_TODAY_DATE          = "������� ����������� ����: ";
	

	private static Scanner scanner = new Scanner(System.in);

	public static String read() {
		return scanner.nextLine().toLowerCase();
	}

	public static void write(String message) {
		System.out.println(message);
	}

	public static String readDate() {
		String date = read().replaceAll("[/\\-]", ".");
		while(!DateMethods.isDateValid(date)) {
			write(INCORRECT_DATE);
			date = read().replaceAll("[/\\-]", ".");
		}

		return date;
	}

	public static Date parseDate(String date) {
		try {
			return DateMethods.parse(date);
		} catch (ParseException pe) {
			write(PARSING_EXCEPTION_THROWED);
			System.exit(0);
			return null;
		}
	}

	public static Date getTodayDate() {
		Date today = new Date(System.currentTimeMillis());
		System.out.printf(IS_TODAY_CORRECT, DateMethods.DATE_FORMAT_POINTS.format(today));
		
		if (!readUserResponse()) {
			write(WRITE_TODAY_DATE);
			today = parseDate(readDate());
		}
		return today;
	}

	public static Date getBirthDate() {
		write(WRITE_YOUR_BIRTH_DATE);
		return parseDate(readDate());
	}

	public static boolean readUserResponse() {
		String response = read();
		return !response.matches("n|no|���");
	}
}